-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
ALTER TABLE projects ADD COLUMN image VARCHAR;

-- +migrate Down
ALTER TABLE projects RENAME TO projects_old;
CREATE TABLE projects (
    id INTEGER PRIMARY KEY,
    owner_id INTEGER NOT NULL REFERENCES owners(id),
    name VARCHAR (255) NOT NULL,
    description VARCHAR (255),
    visibility INTEGER,
    creation_date TIMESTAMP,
    CONSTRAINT uniq_owner_id_name UNIQUE (owner_id, name)
);
INSERT INTO projects SELECT id, owner_id, name, description, visibility, creation_date FROM projects_old;
DROP TABLE projects_old;