/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// SequenceProtocolStore interface for storing testsequence protocols.
type SequenceProtocolStore interface {
	SequenceProtocolAdder
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(projectID, sequenceID string) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(projectID, sequenceID string, protocolID int) (test.SequenceExecutionProtocol, error)
}

// SequenceProtocolAdder interface for storing testsequence protocols.
type SequenceProtocolAdder interface {
	// AddSequenceProtocol adds the given protocol to the store
	AddSequenceProtocol(r *test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion) (err error)
}

// SequenceStartPageGet serves the start page for a testsequence execution.
func SequenceStartPageGet(caseGetter CaseProtocolGetter) http.HandlerFunc {
	printer := &sequenceExecutionPrinter{caseGetter}
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *tsv)
	}
}

// SequenceExecutionPost returns a handler capable of handling every request during the execution
// of a sequence.
func SequenceExecutionPost(caseProtocolLister test.ProtocolLister,
	caseProtocolStore CaseProtocolStore, sequenceProtocolAdder SequenceProtocolAdder, tcg handler.TestCaseGetter,
	caseLister handler.TestCaseLister, taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter,
	activityStore handler.Activities, issueGetter handler.IssueGetter, issueAdder handler.IssueAdder) http.HandlerFunc {

	cp := caseExecutionPrinter{}
	sequencePrinter := sequenceExecutionPrinter{caseProtocolStore}

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.User == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		previous := getFormValueBool(r, keyIsPrevious)

		caseNr := getFormValueInt(r, keyCaseNr)
		stepNr := getFormValueInt(r, keyStepNr)
		caseCount := len(tsv.Cases)

		var progress = sequenceProgress{}
		if err := progress.Init(r, previous); err != nil {
			errors.Handle(err, w, r)
			return
		}
		if !previous {
			if caseNr > 0 && caseNr <= caseCount {
				middleware.AddToContext(r, middleware.TestCaseKey, &tsv.Cases[caseNr-1])
				CaseExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, &progress, tsv, tcg, caseLister, taskAdder, taskGetter,
					activityStore, issueGetter, issueAdder).ServeHTTP(w, r)
				return
			}

			switch {
			case caseNr <= 0 && stepNr == 0:
				handleSequenceStartPage(w, r, cp, tsv)
			case caseNr == 0 && stepNr == 1:
				// Execution finished showing sequence
				http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
			case caseNr == caseCount+1:
				http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
			default:
				executionPageNotFound(w, r)
			}
		} else {
			switch {
			case caseNr == 1 && stepNr == 0:
				sequencePrinter.printStartPage(w, r, *tsv)
			case caseNr > 1 && stepNr == 0:
				middleware.AddToContext(r, keyIsPrevious, true)
				middleware.AddToContext(r, middleware.TestCaseKey, &tsv.Cases[caseNr-2])
				CaseExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, &progress, tsv, tcg, caseLister, taskAdder, taskGetter,
					activityStore, issueGetter, issueAdder).ServeHTTP(w, r)
				c.Case = &tsv.Cases[caseNr-2]
				return
			case caseNr >= 1 && stepNr > 0:
				middleware.AddToContext(r, keyIsPrevious, true)
				middleware.AddToContext(r, middleware.TestCaseKey, &tsv.Cases[caseNr-1])
				CaseExecutionPost(caseProtocolLister, caseProtocolStore,
					sequenceProtocolAdder, &progress, tsv, tcg, caseLister, taskAdder, taskGetter,
					activityStore, issueGetter, issueAdder).ServeHTTP(w, r)
				return
			case caseNr == 0 && stepNr == 1:
				tc := &tsv.Cases[caseCount-1]
				tcv := tc.TestCaseVersions[0]
				middleware.AddToContext(r, middleware.TestCaseKey, tc)
				timerTime, err := getCurrentDuration(r)
				if err != nil {
					errors.Handle(err, w, r)
					return
				}

				cp.printSummaryPage(w, r, tcv, progress.Get()-1, progress.Max(), timerTime, true)
			}
		}
	}
}

func handleSequenceStartPage(w http.ResponseWriter, r *http.Request,
	caseExecutionPrinter caseExecutionPrinter, testSequenceVersion *test.SequenceVersion) {

	caseNr := getFormValueInt(r, keyCaseNr)
	tc := testSequenceVersion.Cases[caseNr]
	tcv := tc.TestCaseVersions[0]

	caseExecutionPrinter.printStartPage(w, r, tc, tcv, 2, totalWorkSteps(*testSequenceVersion), nil, false)
}

func handleSequenceSummaryPage(w http.ResponseWriter, r *http.Request,
	caseExecutionPrinter caseExecutionPrinter, sequenceExecutionPrinter sequenceExecutionPrinter,
	progress progressMeter, testSequenceVersion *test.SequenceVersion, seqPrt *test.SequenceExecutionProtocol,
	protocolLister test.ProtocolLister, seqProtocol *test.SequenceExecutionProtocol,
	taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter) {

	caseNr := getFormValueInt(r, keyCaseNr)
	tc := testSequenceVersion.Cases[caseNr-1]

	if caseNr == len(testSequenceVersion.Cases) { // End of sequence execution
		sequenceExecutionPrinter.printSummaryPage(w, r, *testSequenceVersion, protocolLister, seqProtocol)
	} else { // Print start page of next case
		tc = testSequenceVersion.Cases[caseNr]
		tcv := tc.TestCaseVersions[0]
		caseExecutionPrinter.printStartPage(w, r, tc, tcv, progress.Get(), progress.Max(), seqPrt, false)
	}
}

// saveSequenceProtocol handles the saving of the sequences protocol
// and shows an error modal if something goes wrong
func saveSequenceProtocol(r *http.Request, w http.ResponseWriter,
	sequenceProtocolAdder SequenceProtocolAdder,
	sequenceVersion *test.SequenceVersion,
	seqProtocol *test.SequenceExecutionProtocol,
	protocolLister test.ProtocolLister) {
	// update meta data
	seqProtocol.ExecutionDate = time.Now()
	protocolNr, err := test.GetProtocolNr(protocolLister, seqProtocol.TestVersion)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	seqProtocol.ProtocolNr = protocolNr
	// generate overall result
	seqProtocol.UpdateResult(protocolLister)

	if !contextdomain.GetGlobalSystemSettings().IsExecutionTime {
		seqProtocol.OtherNeededTime = duration.NewDuration(0, 0, 0)
	}

	if err := sequenceProtocolAdder.AddSequenceProtocol(seqProtocol, *sequenceVersion); err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveSummaryPage, r).
			WithLog("Error while trying to save protocol into store.").
			WithCause(err).
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}
}

//GetCurrentSequenceProtocol returns the protocol to the currently running sequence execution to the given request.
//If there is no sequence execution running, the function will return nil, nil
//If an error occurs nil and the error will be returned
func GetCurrentSequenceProtocol(r *http.Request) (*test.SequenceExecutionProtocol, error) {
	seqJson := r.FormValue("seqProtocol")
	if seqJson == "" {
		return nil, nil
	} else {
		protocol := test.SequenceExecutionProtocol{}
		err := json.Unmarshal([]byte(seqJson), &protocol)
		if err != nil {
			return nil, err
		}

		return &protocol, nil
	}
}

//Handler for sequence summary page put
//it executes the save of a test sequence
func SequenceExecutionPut(sequenceProtocolAdder SequenceProtocolAdder,
	protocolLister test.ProtocolLister, testCaseGetter handler.TestCaseGetter, caseProtocolAdder CaseProtocolAdder,
	taskAdder handler.TaskListAdder, taskGetter handler.TaskListGetter, activityStore handler.Activities) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		seqPr, err := GetCurrentSequenceProtocol(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		seqVers, err := handler.GetTestSequenceVersion(r, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		var caseProtocols []test.CaseExecutionProtocol
		caseProtocolsJson := r.FormValue(keyCaseProtocols)
		err = json.Unmarshal([]byte(caseProtocolsJson), &caseProtocols)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		for _, caseProtocol := range caseProtocols {

			caseProtocol.ProtocolNr, err = test.GetProtocolNr(protocolLister, caseProtocol.TestVersion)
			if err != nil {
				errors.Handle(err, w, r)
			}

			caseProtocol.ExecutionDate = time.Now()

			tcv, err := getCaseVersionForProtocol(caseProtocol, testCaseGetter)

			err = caseProtocolAdder.AddCaseProtocol(&caseProtocol, tcv)
			if err != nil {
				errors.ConstructStd(http.StatusInternalServerError,
					failedSave, unableToSaveSummaryPage, r).
					WithLog("Error while trying to save protocol into store.").
					WithStackTrace(1).
					WithCause(err).
					WithRequestDump(r).
					Respond(w)
				return
			}
			seqPr.CaseExecutionProtocols = append(seqPr.CaseExecutionProtocols, id.NewProtocolID(caseProtocol.TestVersion, caseProtocol.ProtocolNr))

		}
		saveSequenceProtocol(r, w, sequenceProtocolAdder, seqVers, seqPr, protocolLister)
		// close the related tasks
		err = setTaskToDone(r, seqVers.ID().TestID, taskAdder, taskGetter, seqPr.SUTVersion, seqPr.SUTVariant)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// Create an activity Log of the sequence execution
		activityEntities := handler.GetContextEntities(r).GetActivityEntities()
		if seqPr.IsAnonymous {
			activityEntities.User = nil
		}
		activityEntities.SequenceProtocol = seqPr
		activityStore.LogActivity(activity.PROTOCOL_SEQUENCE, activityEntities, r)

		// Write url to protocol in response, client will redirect
		url := strings.TrimSuffix(r.URL.Path, "?fragment=true")
		url = strings.TrimSuffix(url, "/execute")
		url = strings.Replace(url, "/testsequences/", "/protocols/testsequences/", 1)
		url = url + "/" + strconv.Itoa(seqPr.ID().Protocol)

		urlJson, err := json.Marshal(url)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(urlJson)
	}
}
