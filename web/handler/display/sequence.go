/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ShowSequenceGet serves the page showing a sequence.
func ShowSequenceGet(commentStore handler.Comments, taskGetter handler.TaskGetter, lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		comments, err := commentStore.GetCommentsForTest(*c.Sequence, c.Project, c.User)
		if err != nil {
			errors.Handle(err, w, r)
		}

		tasks, err := taskGetter.GetTasksForTest(c.Sequence.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ShowSequence(c.Project, c.Sequence, comments, tasks, lister, w, r)
	}
}

// ShowSequence tries to respond with the display page for a sequence
// if an error occurs an error response is sent instead.
func ShowSequence(p *project.Project, ts *test.Sequence, c []*comment.Comment, tasks []*task.Item, lister handler.TestSequenceLister, w http.ResponseWriter,
	r *http.Request) {
	tmpl := GetTabTestSequenceShow(r)
	tsv, err := handler.GetTestSequenceVersion(r, ts)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	// update the SequenceInfo so dynamic data is included
	tsv, err = test.UpdateInfo(tsv)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	var testsequences []string

	sequencelist, err := lister.List(p.ID())
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	for _, sequence := range sequencelist {
		testsequences = append(testsequences, sequence.Name)
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.DeleteLabels, modal.LabelDeleteMessage).
		With(context.Project, p).
		With(context.TestSequence, ts).
		With(context.TestSequenceVersion, tsv).
		With(context.Tasks, tasks).
		With(context.SequenceInfo, tsv.SequenceInfo).
		With(context.Comments, c).
		With(context.DeleteTestSequence, modal.TestSequenceDeleteMessage).
		With(context.TestSequences, testsequences)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestSequenceShow returns either the test sequence show fragment only or the show fragment with all its parent
// fragments, depending on the isFrag parameter
func GetTabTestSequenceShow(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesShowFragment(r)
	}
	return getTabTestSequencesShowTree(r)
}

// getTabTestSequencesShowTree returns the test sequence show tab template with all parent templates
func getTabTestSequencesShowTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		Append("modal/tester-assignment").
		// Tab test sequence show tree
		Append(templates.ShowTestSequence).
		Append(templates.Comments).
		Append(templates.Comment).
		Append(templates.DeleteConfirm).
		Append(templates.ManageLabels).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesShowFragment returns only the test sequence show template
func getTabTestSequencesShowFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.ShowTestSequence).
		Append(templates.Comments).
		Append(templates.Comment).
		Append("modal/tester-assignment").
		Append(templates.DeleteConfirm).
		Append(templates.ManageLabels).
		Get().Lookup(templates.TabContent)
}

// CreateSequenceGet serves the page used to create a new testsequence.
func CreateSequenceGet(caseLister handler.TestCaseLister, sequenceLister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).CreateSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcs, err := caseLister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		var testsequences = make([]string, 0)

		sequencelist, err := sequenceLister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		for _, sequence := range sequencelist {
			testsequences = append(testsequences, sequence.Name)
		}

		tmpl := getTestSequenceNewFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.DeleteLabels, modal.LabelDeleteMessage).
			With(context.Project, c.Project).
			With(context.TestCases, tcs).
			With(context.TestSequences, testsequences), tmpl, w, r)

	}
}

func getTestSequenceNewFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesNewFragment(r)
	}
	return getTabTestSequencesNewTree(r)

}

// getTabTestSequencesNewTree returns the new test sequence tab template with all parent templates
func getTabTestSequencesNewTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence new tree
		Append(templates.NewTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Append(templates.DeleteConfirm).
		Append(templates.ManageLabels).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesNewFragment returns only the new test sequence tab template
func getTabTestSequencesNewFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.NewTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Append(templates.DeleteConfirm).
		Append(templates.ManageLabels).
		Get().Lookup(templates.TabContent)
}

// EditSequenceGet serves the page used to edit a testsequence.
func EditSequenceGet(t handler.TestCaseLister, sequenceLister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).EditSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tmpl := getTabTestSequenceEdit(r)
		latest := len(c.Sequence.SequenceVersions)
		tsv := handler.GetVersion(r, latest)
		if tsv != latest {
			errors.ConstructStd(http.StatusBadRequest,
				handler.ErrInvalidTestSequenceVersionTitle, handler.ErrInvalidTestSequenceVersion, r).
				WithLogf("Client sent invalid case version number %d for editing."+
					"Should've been %d.", tsv, latest).
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		tcs, err := t.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		var testsequences []string

		sequencelist, err := sequenceLister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		for _, sequence := range sequencelist {
			testsequences = append(testsequences, sequence.Name)
		}

		ctx := context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestCases, tcs).
			With(context.TestSequence, c.Sequence).
			With(context.TestSequenceVersion, c.Sequence.SequenceVersions[len(c.Sequence.SequenceVersions)-tsv]).
			With(context.TestSequences, testsequences)
		handler.PrintTmpl(ctx, tmpl, w, r)
	}
}

// getTabTestSequenceEdit returns either the test sequence edit fragment only or the edit fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestSequenceEdit(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesEditFragment(r)
	}
	return getTabTestSequencesEditTree(r)
}

// getTabTestSequencesEditTree returns the test sequence edit tab template with all parent templates
func getTabTestSequencesEditTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.EditTestSequence, templates.TestCaseSelection, templates.ManageVersions, templates.DeleteConfirm).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesEditFragment returns only the test sequence edit template
func getTabTestSequencesEditFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.EditTestSequence, templates.TestCaseSelection, templates.ManageVersions, templates.DeleteConfirm).
		Get().Lookup(templates.TabContent)
}

// HistorySequenceGet serves the page showing the history of a sequence.
func HistorySequenceGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tmpl := getTabTestSequencesHistory(r)
	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, c.Project).
		With(context.TestSequence, c.Sequence)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestSequencesHistory returns either the test sequence history fragment only or the edit fragment
// with all its parent fragments, depending on the isFrag parameter
func getTabTestSequencesHistory(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesHistoryFragment(r)
	}
	return getTabTestSequencesHistoryTree(r)
}

// getTabTestSequencesHistoryTree returns the test sequence history tab template with all parent templates
func getTabTestSequencesHistoryTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence history tree
		Append(templates.TestSequenceHistory).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesHistoryFragment returns only the test sequence history template
func getTabTestSequencesHistoryFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestSequenceHistory).
		Get().Lookup(templates.TabContent)
}
