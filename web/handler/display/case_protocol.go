/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

const (
	errRetrievingProtocols    = "Can't retrieve protocols"
	errRetrievingProtocolsMsg = "The protocols for the testcase could not be retrieved from the store"
)

const (
	errInvalidProtocolNr    = "Invalid protocol nr"
	errInvalidProtocolNrMsg = "The sent protocol number is invalid"
)

// CaseProtocolsGet serves the page displaying case protocols.
func CaseProtocolsGet(protocolLister handler.CaseProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil || c.CaseProtocol == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		// Get the caseVersion for this protocol
		var caseVersion test.CaseVersion
		for _, version := range c.Case.TestCaseVersions {
			if version.VersionNr == c.CaseProtocol.TestVersion.TestVersion() {
				caseVersion = version
				break
			}
		}

		selType := r.FormValue(httputil.ProtocolType)
		selCase := r.FormValue(httputil.SelectedProtocol)
		selSequence := r.FormValue(httputil.SelectedProtocol)

		tmpl := getTabProtocolsShowTestCaseTree(r)
		if httputil.IsFragmentRequest(r) {
			tmpl = getTabProtocolsShowTestCaseFragment(r)
		}

		var executionTime duration.Duration
		for _, stepProt := range c.CaseProtocol.StepProtocols {
			executionTime = executionTime.Add(stepProt.NeededTime)
		}
		executionTime = executionTime.Add(c.CaseProtocol.OtherNeededTime)

		ctx := context.New().
			WithUserInformation(r).
			With(context.SelectedTestCase, selCase).
			With(context.SelectedTestSequence, selSequence).
			With(context.SelectedType, selType).
			With(context.Project, c.Project).
			With(context.Protocol, c.CaseProtocol).
			With(context.TestCase, c.Case).
			With(context.TestCaseVersion, caseVersion).
			With(context.ExecutionTime, executionTime)
		handler.PrintTmpl(ctx, tmpl, w, r)
	}
}

// getTabProtocolsShowTestCaseFragment returns only the protocol show test case tab template
func getTabProtocolsShowTestCaseFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestCaseProtocols).
		Append(templates.PrintMarkdown).
		Get().Lookup(templates.TabContent)
}

// getTabProtocolsShowTestCaseTree returns the protocol show test case tab template with all parent templates
func getTabProtocolsShowTestCaseTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.TestCaseProtocols).
		Append(templates.PrintMarkdown).
		Get().Lookup(templates.HeaderDef)
}
