/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"

	"regexp"
	"strconv"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

const (
	failedRegister          = "Registration failed"
	unableToRegisterNewUser = "We were unable to register you. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
	accountNameAlreadyExists = "We were unable to register you. Your chosen username is already assigned."
	emailAlreadyInUse        = "We were unable to register you. Your chosen email address is already in use."
	nameNotAllowed           = "We were unable to register you. The username is not allowed."
	displayNotAllowed        = "We were unable to register you. The displayname is not allowed."
)

// register is the handler for register requests from the client.
type register struct {
	server RegisterServer
}

// RegisterServer is used to register a new user
type RegisterServer interface {
	Get(id id.ActorID) (*user.User, bool, error)
	GetByMail(email string) (*user.User, bool, error)
	Add(pur *user.PasswordUser) error
}

// NewRegisterHandler creates a new signout handler that handles signout requests from the client.
func NewRegisterHandler(server RegisterServer) http.Handler {
	return &register{server}
}

// ServeHTTP handles register requests from the client.
func (reg *register) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	display := r.FormValue(httputil.DisplayName)
	account := r.FormValue(httputil.UserName)
	email := r.FormValue(httputil.Email)
	password := r.FormValue(httputil.Password)
	isAdminString := r.FormValue(httputil.IsAdmin)
	isAdmin := false

	//Trim all leading and trailing whitespaces and replace multiple whitespaces with a single space character
	account = strings.TrimSpace(account)
	re := regexp.MustCompile(`\s\s+`)
	account = re.ReplaceAllString(account, " ")

	// Only check for empty User if Self-Registration is disabled
	if !contextdomain.GetGlobalSystemSettings().IsRegistrationAllowed {
		if c.User == nil {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}
	} else {
		if c.User != nil {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}
	}

	isAdmin, err := strconv.ParseBool(isAdminString)
	if err != nil {
		errors.Handle(err, w, r)
	}

	if isAdmin {
		if !c.User.IsAdmin {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}
	}

	u, err := reg.register(account, email, display, password, isAdmin)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	w.Header().Add(httputil.NewAccount, u.Name)
	w.WriteHeader(http.StatusCreated)
}

// register tries to register a user with given data. If the registration or request fails
// an error is returned and the returned user is nil otherwise the error is nil
// and the user is the freshly registered user.
func (reg *register) register(name, email, display, password string, isAdmin bool) (*user.User, error) {
	name = strings.ToLower(name)
	userID := id.NewActorID(name)
	if u, exists, err := reg.server.Get(userID); exists && u.Name == name {
		if err != nil {
			return nil, err
		}
		return nil, errors.ConstructStd(http.StatusConflict,
			failedRegister, accountNameAlreadyExists, nil).
			Finish()
	}

	displayName := strings.ToLower(display)
	if name == "anonymous" || name == "deleted" {
		return nil, errors.ConstructStd(http.StatusConflict, failedRegister, nameNotAllowed, nil).Finish()
	}
	if displayName == "anonymous" || displayName == "deleted" {
		return nil, errors.ConstructStd(http.StatusConflict, failedRegister, displayNotAllowed, nil).Finish()
	}

	if u, exists, err := reg.server.GetByMail(email); exists && u.Email == email {
		if err != nil {
			return nil, err
		}
		return nil, errors.ConstructStd(http.StatusConflict,
			failedRegister, emailAlreadyInUse, nil).
			Finish()
	}

	// Create new user
	newUser := user.New(display, name, email, isAdmin)
	// Associate password and user
	pwUserRel := user.PasswordUser{
		Password: password,
		User:     newUser,
	}

	// Add new user with password to the store
	err := reg.server.Add(&pwUserRel)
	if err != nil {
		return nil, errors.ConstructStd(http.StatusInternalServerError,
			failedRegister, unableToRegisterNewUser, nil).
			WithLog("Failed to add user to the persistence.").
			WithStackTrace(1).
			WithCause(err).
			Finish()
	}

	return &newUser, nil
}
