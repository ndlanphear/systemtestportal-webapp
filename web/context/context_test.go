/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package context

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store"

	"net/http/httptest"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// TestNew test the new function for the context package.
func TestNew(t *testing.T) {
	c := New()

	k := "key"
	v := "test"
	c[k] = v

	if c[k] != v {
		t.Errorf("Context doesn't work like a map: "+
			"Expected %s but got %s for key %s.", v, c[k], k)
	}
}

// TestContext_With tests the With function for a context object returned
// by context.New()
func TestContext_With(t *testing.T) {
	k := "key"
	v := "test"
	c := New().With(k, v)

	if c[k] != v {
		t.Errorf("context.Context.With() value wasn't assigned correctly:"+
			" Expected %s but got %s for key %s.", v, c[k], k)
	}
}

// TestContext_WithUserInformation tests the WithUserInformation function for
// a context object returned by context.New()
func TestContext_WithUserInformation(t *testing.T) {
	store.InitializeTestDatabase()

	u := user.New("ImaDummy",
		"ImanoDummy",
		"ima@example.com", false)
	r := httptest.NewRequest(http.MethodPost, "/", nil)
	var tests = []struct {
		rUser        *user.User
		rUserPresent bool
		eUser        *user.User
		eSignedIn    bool
	}{
		{
			nil,
			false,
			nil,
			false,
		},
		{
			nil,
			true,
			nil,
			false,
		},
		{
			&u,
			true,
			&u,
			true,
		},
	}
	for _, tc := range tests {
		if tc.rUserPresent {
			middleware.AddToContext(r, middleware.UserKey, tc.rUser)
		}
		c := New().WithUserInformation(r)

		if c[SignedInKey] != tc.eSignedIn {
			t.Errorf("context.Context.WithUserInformation() doesn't set "+
				"values correctly: Expected %t but was %t for key %s", tc.eSignedIn,
				c[SignedInKey], SignedInKey)
			t.Logf("Failed for test-case %+v", tc)
		}
		u, ok := c[UserKey]
		if !eq(u, ok, tc.eUser) {
			t.Errorf("context.Context.WithUserInformation() doesn't set "+
				"values correctly: Expected %+v but was %+v for key %s", tc.eUser,
				u, UserKey)
			t.Logf("Failed for test-case %+v", tc)
		}
	}
}

// eq checks if a map entry is equal to a given other value.
// Equality is defined the following way:
// Nil is equal to no entry in the map (ok is false).
// Otherwise the values are equal if they are equal by the '=='-operator.
func eq(u interface{}, ok bool, eu *user.User) bool {
	return (ok && u == eu) || (!ok && eu == nil)
}


