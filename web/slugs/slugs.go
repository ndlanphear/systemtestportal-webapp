/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package slugs

import (
	"net/url"
	"strings"
)

//URLEncode encodes the given string.
//Before use be sure, that you need to encode this string and isn't encoded already somewhere else like in an library.
//Encoding a string twice will cause problems
func URLEncode(s string) string {
	result := url.QueryEscape(s)
	//Adjust coding of whitespaces.
	return strings.Replace(result, "+", "%20", -1)
}

//URLDecode decodes the given string.
//Be sure that the given string is encoded.
//Decoding a non-encoded string will cause problems.
func URLDecode(s string) (string, error) {
	return url.QueryUnescape(s)
}
