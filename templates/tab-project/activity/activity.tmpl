{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Activity Details" . }}Activity Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "This sections shows all recent activity of this project. Click on one of the Icons to filter all activities of this type." . }}
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <div id="sliderHelp" class="stp-slider" style="margin-top: 1.5rem"></div>
                            <script>
                                $('#sliderHelp').slider({
                                    orientation: "horizontal",
                                    range: "min",
                                });
                            </script>
                        </td>
                        <td>{{T "Increase or decrease the distance of the elements in the activity Tree." . }}</td>
                        <td>
                            <button class="btn btn-light d-none d-sm-inline border-dark" disabled>Plus / Minus</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="timeline-badge-help info"><i class="fa fa-info text-white"></i></div>
                        </td>
                        <td>{{T "Information about a general event." . }}</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="timeline-badge-help success"><i class="fa fa-check text-white"></i></div>
                        </td>
                        <td>{{T "Successful tests executed by users." . }}</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="timeline-badge-help warning"><i class="fa fa-exclamation text-white"></i></div>
                        </td>
                        <td>{{T "Partial successful tests executed by users." . }}</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="timeline-badge-help danger"><i class="fa fa-exclamation text-white"></i></div>
                        </td>
                        <td>{{T "Failed tests executed by users." . }}</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="timeline-badge-help info"><i class="fa fa-question text-white"></i></div>
                        </td>
                        <td>{{T "Not assessed tests executed by users." . }}</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">documentation</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="tab-card card" id="tabMembers">

    <nav class="navbar-light action-bar p-3">
        <div class="row" style="height: 100%;width: 100%;">
            <div class="col">
                <h4 class="mb-3 float-left" style="margin-top: 0.75rem;">{{T "Activity" .}}</h4>
            </div>
            <div class="col-4"></div>
            <div class="col-6" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Change the spacing between activity items">
                <div id="slider" class="stp-slider" style="margin-top: 1.5rem"></div>
            </div>
        </div>
    </nav>

    <div class="container">

        {{ if not .Activities}}

            <p>
                <p class="mb-3 text-muted"  style="text-align: center;">
                    {{T "No activity yet... time to start!" . }}
                </p>
            </p>

        {{ else }}

            <ul class="timeline" id="timeline-list" style="margin-bottom: 4rem">
            {{range .Activities}}
                {{ .GetRenderedItemTemplate $.Language}}
            {{end}}
            </ul>

            <ul class="timeline" id="loader-timeline" style="margin-bottom: 4rem; display:none">
                <li>
                    <div class="timeline-badge loader"/>
                </li>
            </ul>

        {{ end }}
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/activity.js" integrity="{{sha256 "/static/js/project/activity.js"}}"></script>

<script>
    $("#printerIcon").addClass("d-none");
    $("#helpIcon").removeClass("d-none");

    $( function() {
        $( "#slider" ).slider({
            orientation: "horizontal",
            range: "min",
            min: -2.5,
            max: 2.5,
            value: -2.5,
            step: 0.1,
            slide: updateMargin,
            change: updateMargin
        });
    } );

    $("time.timeago").timeago();
    initDataTree();
    $(".activity-tooltip").tooltip();
</script>
{{end}}