{{define "modal-manage-versions"}}

{{template "modal-delete-confirm" .DeleteVersions}}

<!-- Unsaved Changes -->
<div class="modal fade" style="z-index: 9999" id="unsavedChangesVersionModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Exiting with unsaved changes" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Some changes are not saved yet. Continue?" .}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"
                        id="buttonApplyVersionChangesClosing">{{T "Apply Changes" .}}</button>
                <button type="button" class="btn btn-danger"
                        id="buttonDiscardVersionChangesClosing">{{T "Discard Changes" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Abort" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- unsaved changes switching -->
<div class="modal fade" style="z-index: 9999" id="unsavedChangesVersionModalSwitching">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Switching versions with unsaved changes" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Some changes are not saved yet. Continue?" .}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"
                        id="buttonApplyVersionChangesSwitching">{{T "Apply Changes" .}}</button>
                <button type="button" class="btn btn-danger"
                        id="buttonDiscardVersionChangesSwitching">{{T "Discard Changes" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Abort" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- main modal-->
<div class="modal fade" id="modal-manage-versions" tabindex="-1" role="dialog"
     aria-labelledby="modal-manage-versions-label" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 37.5rem;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-manage-versions-label"><i class="fa fa-wrench" aria-hidden="true"></i>
                {{T "Manage Versions and Variants" .}}</h5>
                <button id="topButtonCloseManageVersionsModal" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <wrapper>
                <div class="modal-body" style="width: 40%;float: left;border-right: 1px solid #e9ecef;">
                    <button id="buttonNewProjectVersion" class="btn label-list-btn" type="button"
                            style="opacity: 1; font-size: 1.25rem; margin-bottom: 5px;color: #fff;background-color: #5d6770;border-color: #5d6770;">
                    {{T "New Version" .}}
                    </button>
                    <br>
                    <h5>{{T "Versions" .}}</h5>
                    <div class="row" style="overflow: auto; max-height: 20.625rem;">
                        <div class="col-12">
                            <p id="projectVersionList">
                            </p>
                        </div>
                    </div>
                    </ul>
                </div>
                <div class="modal-body" style="width: 60%;float: right;border-left: 1px solid #e9ecef;">
                    <h5 id="modeVersionApplicabilityNew">{{T "New Applicability" .}}</h5>
                    <h5 id="modeVersionApplicabilityEdit" style="display: none">{{T "Edit Applicability" .}}</h5>
                    <form id="form-label-inputs">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">{{T "Version Name" .}}</span>
                            </div>
                            <input type="text" class="form-control" id="inputProjectVersionName"
                                   pattern="[\sA-Za-z0-9-_().,:äöüÄÖÜß]{1,256}"
                                   placeholder="e.g. 2018.6.6">
                        </div>
                    </form>
                    <h5>{{T "Variants" .}}</h5>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputProjectVariant"
                                       pattern="[\sA-Za-z0-9-_().,:äöüÄÖÜß]{1,256}">
                                <span class="input-group-prepend">
                                    <button id="buttonAddProjectVariant" class="btn btn-primary"
                                            style="border-bottom-right-radius: 0.25rem; border-top-right-radius: 0.25rem;"
                                            type="button">{{T "Add Variant" .}}</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row"
                         style="border: 1px solid #ced4da; border-radius: 0.25rem;
                          height:12.5rem; margin: 5px 0 5px 0; overflow-y: scroll; overflow-x: hidden">
                        <div class="col-12" style="padding: 5px">
                            <form id="form-variant-input" onsubmit="return false;">
                                <p id="projectVariantList">
                                </p>
                            </form>
                        </div>
                    </div>
                    <button id="buttonDeleteVersion" type="button" class="btn btn-danger" data-toggle="modal"
                            data-target="#deleteVersions" disabled>{{T "Delete Version" .}}
                    </button>
                    <button id="buttonAddVersions" type="button" class="btn btn-primary"
                            style="float: inherit;">{{T "Add" .}}
                    </button>
                    <button id="buttonSaveVersions" type="button" class="btn btn-primary"
                            style="float: inherit; display: none;">{{T "Save" .}}
                    </button>
                    <br>
                </div>
            </wrapper>
            <div class="modal-footer">
                <button id="bottomButtonCloseManageVersionsModal" type="button"
                        class="btn btn-secondary">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>
<script src="/static/js/misc/sut-version.js" integrity="{{sha256 "/static/js/misc/sut-version.js"}}"></script>
<script type='text/javascript'>
</script>
{{end}}