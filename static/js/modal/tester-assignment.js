/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

// The assignments that are supposed to be deleted.
// They are stored in a map (assignees) of arrays (indices of the task items)
var doneTasks = {};

$("#buttonAssignTester")
    .off("click")
    .on("click", () => {
        sendTester(getSelectedUsers(".testerSelector"));
        $('#modal-tester-assignment').modal('hide');
    });

/**
 *  Fetches the selected users from checkboxes and
 *  version and variants from dropdowns
 */
function getSelectedUsers(selector) {
    const tester = [];
    var versions = [];
    var variants = [];
    $(selector).each(function () {
        if(this.checked) {
            tester.push(this.name);
            versions.push($("#assignmentVersions-"+this.name).val());
            variants.push($("#assignmentVariants-"+this.name).val());
        }
    });

    return {newtesters: JSON.stringify(tester),
            versions: JSON.stringify(versions),
            variants: JSON.stringify(variants),
            doneTasks: JSON.stringify(doneTasks)};
}

/**
 * Sends the new assignments to the server. Upates the list with assignees.
 * @param assignmentData (Testers, versions and variants)
 */
function sendTester(assignmentData) {

    const urlSeg = window.location.pathname.split("/");
    const url = `${urlSeg[0]}/${urlSeg[1]}/${urlSeg[2]}/${urlSeg[3]}/${urlSeg[4]}/tester`;

    $.ajax({
        url: url,
        type: "PUT",
        data: assignmentData,

    }).done(() => {

        // Reload page. Otherwise we would have to request the
        // new data from the server to display the updated list in a test.
        // This also has the advantage that the task-number in the navbar
        // of a user is directly updated.
        location.reload();

    }).fail(response => {
        $("#modalPlaceholder").empty().append(response.responseText);
        $('#errorModal').modal('show');
    });
}

/**
 * Listens to a click event on a user in the list of assignees.
 * When a tester is selected, show the dropdowns with version and variant.
 */
$(".testerSelector").on("click", function () {
    if (this.checked) {
        $("#assignmentVersionVariant-" + this.id).show();
    } else {
        $("#assignmentVersionVariant-" + this.id).hide();
    }
});

/**
 * Listener for deleting an assignment out of the list of
 * all assignments for a test.
 * Removes the assignment from the list.
 */
$(".deleteAssignment").on("click", function () {
    var id = this.id;
    // Create the id of the div that contains the assignment
    var divID = "#a" + id.slice(7);
    $(divID).css("opacity", 0.6);
    // Set the index of the task item to delete
    var testerName = divID.split("-")[1];
    if (!doneTasks[testerName]) {
        doneTasks[testerName] = [];
    }
    doneTasks[testerName].push(divID.split("-")[2]);
});