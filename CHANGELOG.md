# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Downloads

Releases of the SystemTestPortal can be downloaded from the [tags](https://gitlab.com/stp-team/systemtestportal-webapp/tags).
Nightly builds can be found on the [FTP Server of the University of Stuttgart](ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/).


## [v2.0.0] - unreleased (first release candidate on 2018-10-11)

### Added
- Added relevant execution protocols to the response for CI/CD integration
- Added means to capture and export performance metrics in the Prometheus format on the /metrics endpoint
- Added the activities tab.
- Added license page for STP's license, linked in about and licenses
- Added dialog to change the password to the user settings
- Added keyboard shortcuts to help modals
- Added tooltip to help button and print button
- Added link to our documentation to the help modals
- Added Configurable Imprint and privacy policy
- Added custom profile pictures to users
- Added a new label assignment- field in test creation/show
- Added the label edit modal to test creation/show
- Added option to hide project description and image
- Added to dashboard: possibility to sort by column, possibility to search by input query
- Added integration to gitlab to create an issue when a test fails
- Added option to allow or disallow general access
- Added dashboard for user
- Added validation for test case, test sequence, user and project names
- Added trimming for test case, test sequence, user and project names
- Users can now deactivate their account
- Admins can toggle between anonymizing and maintaining data upon user deactivation
- Finishing an execution now forwards to the respective protocol
- Added configuration options for security headers to the config.ini file 
- Added SRI to all served pages
- Added option to turn off execution timers globally

### Changed
- Duplicating test cases and test sequences now adds a separate version to the history
- The 'Groups' section on the Explore page is now hidden
- Changed the 'help' stp flag to no longer show information about go's 'test' flag
- Changed the register page to show 'Register' when an anonymous user wishes to register
- Changed the member removal to show a warning modal when trying to remove a user with active assignments
- Changed the member remove modal to make users with active assignments red
- Changed the profile settings to allow users to change their profiles without entering their passwords.
- Changed labels fundamentally. They are no longer compatible with labels pre 2.0
- Changed the functionality of the back button to an more intelligent version
- Change the default handling for the assignment of a version to the tests for the integration.
- Instead of having no assigned tests, now all tests that were applicable to the previous version are applicable to the new version.
- Enabled checkboxes for self registration and general access
- Changed usernames to be lowercase only
- Changed minimum length of username and displayname to 4
- Changed the Manage Versions and Variants dialog
- Admins now have all permissions, without being part of a project
- It is no longer possible to assign the same user to the same version/variant of the same test multiple times
- The last version is now selected by default
- Updated the logo with a new version contributed by [Peter Werner](https://gitlab.com/SilentTeaCup)
- Changed the headings for the user and project lists to be more consistent

### Fixed
- Fixed assignment modal for testcases/testsequences without applicable versions
- Fixed an issue where buttons in the project tab where not squashed into 'more' correctly
- Users that are not logged in can no longer add/remove labels from cases/sequences
- Fixed the assignment modal versions being empty for test sequences
- Fixed the assignee styling for test sequences to be consistent with the test case assignee styling
- Fixed UI consistency, added background cards to all templates
- Fixed display issue of test steps in testcase execution summary page
- Fixed an issue where the print and help icon would break into a new line
- Fixed duplicate attached key listeners to modal-key management
- Fixed "back" shortcut on execution
- Fixed creating a sequence without a test case
- Fixed display of testers in protocols set for anonymous execution
- Fixed missing help/print icon if you clicked on a testcase/sequence/protocoll in the dashboard
- Fixed the applicability for new test cases
- It is impossible now to execute tests that are not applicable.
- Fixed testcases creation if there are no other test cases
- Fixed an issue with lists being empty on Nexus 9 tablets

## [v1.7.0] - 2018-10-14 (first release candidate on 2018-08-21)

### Added
- Added an import for projects
- Added possibility to jump from sequence protocols to nested case protocols
- Added integration into the ci/cd. See gitlab.com/schneisn/stp-ci-integration for more information.
- Added Task count badge in navigation bar
- Added Expand/Collapse functionality for test case protocols
- Added a tooltip when checking preconditions during the start of an execution
- Assignees can now be unassigned
- Added possibility to reorder steps in a test case via drag & drop
- Added expand and collapse all buttons to test steps in test cases
- Added pagination and possibility to order protocols by column
- CI/CD Integration: Tests for a version are automatically assigned to the testers in a project
- Added possibility to toggle tasks and reopen tasks in task list
- Completing a task assignment now automatically closes the related task
- Added Project Image Input in Project creation
- Added Profile Page
- Added Account Settings
- Added duplication of test sequences
- Added option to turn on self-registration in the system settings
- Added functionality for exporting protocols to Markdown
- The project export JSON now contains test sequences
- Added variant- and version-selection to task-assignment

### Changed
- Change wording on register page if not signed in
- The Execution time is now shown in test protocols
- Changed images to use rounded corners instead of sharp ones
- Update Bootstrap from 4.0.0 beta to 4.1.3
- Raised comment character limit to 1000 characters
- Use icons instead of text for the navigation bar
- Updating the count of undone tasks on each request
- Register buttons are now icons
- Update Copyright to 2017-2018
- Changed the partially passed icon from a triangle to a circle
- Updated the layout for exported protocols
- Clicking on a protocol now opens that protocol


### Fixed
- Restrict allowed characters to `pattern=[A-Za-z0-9-_().,:äöüÄÖÜß]{1,{{$maxNameLength}}}`
- Fixed label filtering when certain characters were used
- Fixed wrong date formatting in protocol's detail section
- Test sequence protocol is now not saved, till you press finish
- Test sequence execution time is now saved correctly
- Test case execution time is now saved correctly
- Fixed duplicating of test cases
- Assignees are now persistent
- Fixed an incorrect error message when deleting duplicated test cases
- Removed register button from system settings
- Dashboard errors now use proper modals instead of alerts
- Fixed adding images to step results if text had already been entered
- Fixed a bug where renaming a label would remove it from its test cases and test sequences
- Fixed display of the footer in the system settings

## [v1.6.0] - 2018-09-18 (first release candidate on 2018-08-04)

### Added
- Added a system settings page.
- Added General Setting to the system settings page.
- Added a Global Message. This global message is configurable in the system settings page.
- Added search for projects, groups and users
- Added overall keyboard support
- Comments are now editable
- Added sort and filter functionality to test cases and sequences.
- You can now add images to actual result text fields
- Added pagination to exploration pages

### Changed
- New created Test cases are now applicable to all versions and variants
- Comments are now stored persistently
- Improved UX of comments
- Improved precondition layout
- Changed "Todo" to "Task"

### Fixed
- Fixed a bug, where clicking on a protocol in the protocols list, nothing happened
- Fixed a bug when executing a test sequence with a test case, that has no steps
- Fixed a bug where the protocol filter wasn't working right

### Security
- Fixed XSS vulnerability in applicability
- Fixed XSS vulnerability in user assignees
- Fixed XSS vulnerability in labels
- Removed the ability to registrate a user wihtout beeing logged in

## [v1.5.0] - 2018-08-14 (first release candidate on 2018-07-25)

### Added
- Added system wide admins. An admin has the option to add additional admins on the create user screen.
- Added protocol list for all testcases/testsequences
- Added Markdown support to most textfields

### Changed
- Changed the styling of the switch between cases and sequences in the dashboard, so it is not misleading.
- Change protocol PDF button text to "Export to PDF"
- Stopped using cookies for the test executions
- Changed the assigned tester display from labels to a list
- Change "Explore" text in navbar to "Projects"
- Changed the styling of the switch between cases and sequences in the dashboard, so it is not misleading.
- Changed the permission settings layout


## [v1.4.0] - 2018-08-07 (first release candidate on 2018-07-07)

### Added
- Added the Permission settings page, where you can edit the permissions of existing roles, add new roles and delete roles
- Tasks for users. Tasks are created when assigning test-cases and test-sequences
- Added option to anonymously execute test cases and test sequence
- Projects can now be exported to json

### Changed
- You need to be logged in now, to add a new user to the STP
- Removed RAM build option
- Change case/sequence execution layout
- Redesign of lable editing dialog
- Improved example database with test protocols

### Fixed
- An error, when you create a testsequence with the same testcase added multiple times

## [v1.3.0] - 2018-07-29 (first release candidate on 2018-06-28)

### Added
- Preconditions are now itemized
- Added label editing functionality
- Test Sequence protocols can now be exported to pdf
- Test Case protocols can now be exported to pdf
- Added the ability to reorder testcases of a testsequence when editing a testsequence
- Added tooltip to labels, which shows the description of the label
- Added a delete confirmation pop-up when deleting labels
- Allow the tester to choose between three levels (fulfilled, partially fulfilled, not fulfilled) for each precondition before starting a test execution
- Added confirm dialog on abort events when executing tests

### Changed
- Changed result "Passed with Comment" to "Partially Successful"
- Redesigned the Manage Label dialog
- Limited the label length to 2000 characters

### Fixed
- cell tooltips are now displayed correctly after changing the variant


## [v1.2.0] - 2018-06-16

### Added
- Clicking on a test case cell on the test case dashboard now links the related test case page
- Clicking on a test sequence cell on the test sequence dashboard now links the related test sequence page
- Testcase and Testsequence protocols are now saved persistent in the database
- Added basic tooltip to dashboard results, which shows tester, date, comment and result of a test case
- Visibility settings for projects
- Support for storing and display project images
- Clicking on a test case result cell on the test case dashboard now links to the related test case protocol
- Added highlighting of the selected label color in the label creation
- Select role of user when adding a member to a project
- The role of a member is now shown in the member tab
- Added testsequence dashboard, you can switch between testcase and testsequence dashboard via button
- Added label editing functionality
- If you have the permission you can now edit the role of members on the members page.

### Changed
- Limited the label length to 50 characters to prevent formatting and layout problems
- Invert Version and Variant

### Fixed
- Issue #472 in which it was not possible to add versions to variants after saving a variant without a version
- Refreshing start page in test-sequence execution did not show the user as signed-in (Issue #348)
- Users can now delete variants and versions
- Adding a test cases multiple times to the same sequence is now possible 
- A bug in which the selected label color did not reset after adding a new label

### Removed
- Removed help button from settings and dashboard page, since there is nothing to do for now


## [v1.1-beta] - 2018-05-29

### Added
- Pause Button now changes icon when clicking it
- Added Dropdown-Menu for dashboards, where you can switch between variants
- The Executioner of "Test cases" and "Test sequences" now gets saved and shown in Protocols.

### Fixed
- The delete buttons at "Manage variants and versions"
- Issue #354 in which test step deletion button only worked on the edge of the button


## [v1.1-alpha] - 2018-05-21

### Added
- Backend and frontend support for colored labels in "Test Cases".
- Backend and frontend support for colored labels in "Test Sequences".
- Added a basic and simple dashboard for "Testcases"

### Changed
- Improve usability of the manage variants and versions dialog
- Use the persistent sqlite version as default, to build with the volatile ram version now use ```go build --tags 'ram' <path-to-/cmd/stp>```

### Fixed
- Issue #407 in which labels were no longer clickable after pressing "Save Settings"

## [v1.0.0-rc4] - 2018-05-07

### Added
- Backend support for User Roles and User Premissions

### Fixed
- Issue #363 which prevented users from saving test cases
- Fixed some displaying on mobile devices

## [v1.0.0-rc3] - 2018-05-06

### Changed
- Filter test cases/test sequences with multiple labels

### Fixed
- Fixed an issue where the testsequence summary page would show incorrect times
- Fixed "Enter-key" for textfields
- When editing, adding sut-versions to a case that contained no sut-versions caused the server to panic. This is fixed now
- Adding more test-cases to a test-sequence on edit now works as intended
- Replace empty dropdown-menu with selected variant+version in start-pages of test-cases when executing a test-sequence
- Fix updating the selected sut-variants and sut-versions when editing a test-case
- Generate the sequence info (sut-versions / duration) before displaying them, fixing it being empty when using the sqlite version and the user being unable to execute a test sequence
- Adding and removing members from a project when using the persistent 'sqlite' version


## [v1.0.0-rc2] - 2018-04-16

### Fixed
- Fix execution of test-sequences. Previously only the first case could be executed
- Clicking on an older version in the history of a test now requests the correct url
- Clicking on the version of a test in a test-protocol now requests the correct url
- Show new-project button on explore-projects page if you are signed in
- When creating new project, the owner is added to the members and can add/remove members
- Fix computing the protocol id for newly created protocols

## [v1.0.0-rc1] - 2018-03-31

### Added
- Projects have members now

### Fixed
- Fix bug where an empty test step was created for every manually entered test step on test case creation

## [v1.0.0-beta] - 2018-03-27

### Added
- Show the needed time for testing in the summary page of a text execution
- Add Architectural Decision Records (https://adr.github.io/)
- Test for handlers
- Test plan to manually test the SystemTestPortal

### Changed
- Only renaming a test does not create a new test version anymore because there would not be any visible changes in the history of a test
- Url encoding for projects and tests
- Remember which protocol was selected
- By default, the support for sqlite is disabled due to issues with broken binaries (See #223)

### Fixed
- Fix binaries not starting anymore (See #223)
- Fix registration of users
- Fix disappearing protocols after renaming a test case
- Fix a bug that did not allow the name of a test case being changed back to its previous name
- Show correct results of executed test sequences in protocols

### Known issues
- Test sequences without any applicable sut-versions can't be executed (#239)
- During an test sequence execution the start page of each test case shows an empty dropdown instead of the selected sut-variant + sut-version (#236)

## [v0.11.0] - 2018-02-13

### Added
- Add persistent storage
- Add docker image

### Fixed
- Fix test sequence protocols showing "Invalid Date" for execution date
- Fix listing of projects. Now all projects can be listed on the explore page
- Fix wording of error when test sequence cannot be updated. The error does not show "Can not update test case" anymore.

### Known issues
- Registering a user does not work
- Renaming a project does not work correctly
- Protocols of test sequences show result as "Not assessed" altough there is a result
- The urls are sometimes encoded wrong

## [v0.10.0] - 2018-01-30

### Added
- Add comments for test cases and test sequences
- Add settings for projects
- Improved GUI

### Fixed
- Automatically update the list of sut-versions with the text when adding a sut-version to a project containing no sut-versions
- Only update the labels when clicking "save" instead of updating them when the modal closes
- Fix missing characters in the protocol title (e.g. a missing "ß")
- Trying to sign in with a wrong password now shows a full error page

### Security
- Add hashing of passwords in the database

### Known issues
- Protocols of test sequences contain the wrong test case protocols
- Not all projects are listed in the explore tab. Only the projects of the "default" user are listed
- Renaming or deleting a test prevents its protocols from being listed

## [v0.9.0] - 2018-01-23

### Added
- Add a print dialog for the list of test cases and test sequences
- Add a print dialog for the detailed view of test cases and test sequences
- Add a print dialog for test cases and test sequences with free input forms to execute the tests with pen and paper
- Add option to delete a variant of the sut
- Add labels for test cases and test sequences

### Changed
- Move hardcoded strings (paths, parameters, keys) to constants
- Wrap up ids of projects, groups, tests, users

## [v0.8.0] - 2018-01-08

### Added
- Add a timer to track the time needed for a test execution
- Add system-under-test variants in addition to the existing system-under-test-versions
- Use https://github.com/dimfeld/httptreemux as a router for the requests

### Fixed
- Fix whitespace in the textarea of the commit-message when editing tests

## [v0.6.0] - 2017-11-24

This is the first public release of the SystemTestPortal. Basic features such as the creation of projects, test cases and test sequences and the execution are implemented.
Additionally, there is a basic concept of users and permission management. Some features are restricted to signed-in users.

### Added
- Simple registration process
- Features limited to signed-in users (e.g. creating tests)
- Add creation of single tests and compound test sequences
- Add editing of tests and test sequences
- Add history for tests. Older versions of a test can be shown but not edited
- Add execution for tests and test sequences
- Add protocols to show results of executed tests and test sequences
